package co.simplon.promo27.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import co.simplon.promo27.component.ContextMenu;
import co.simplon.promo27.component.FileDisplay;
import co.simplon.promo27.model.Element;
import co.simplon.promo27.model.Explorer;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;

/**
 * Classe "contrôleur" permettant d'assigner des valeurs et des actions à la vue
 * FXML principale.
 * 
 * @see La vue associée src/main/resources/co/simplon/promo27/primary.fxml
 * 
 */
public class PrimaryController {

    private Explorer explorer = Explorer.getInstance();
    private Element selected = null;

    @FXML
    private ContextMenu contextMenu;
    @FXML
    private GridPane container;
    @FXML
    private Label currentPosition;

    /**
     * Méthode exécutée à l'affichage de la page principale
     */
    @FXML
    private void initialize() {
        updateDisplay();
    }

    /**
     * Méthode permettant de mettre à jour l'affichage à chaque modification des
     * propriétés, de l'emplacement
     * de l'explorateur ou autre.
     */
    @FXML
    private void updateDisplay() {
        contextMenu.setElement(selected);

        List<Element> content = explorer.getContent();

        content
                .stream()
                .sorted((object1, object2) -> object1.getName().compareTo(object2.getName()));

        container.getChildren().clear();
        int row = 0;
        int col = 0;
        for (Element element : content) {
            FileDisplay fileDisplay = new FileDisplay(element);
            /**
             * Assignation des actions au click ou double click sur un fichier
             */
            fileDisplay.setOnMouseClicked((MouseEvent event) -> {

                if (event.getClickCount() == 2) {
                    if (element.isDirectory()) {
                        selected = null;
                        explorer.changeLocation(element.getName());

                    }
                }
                if (event.getClickCount() == 1) {
                    selected = element;
                }
                updateDisplay();
            });

            if (selected == element) {
                fileDisplay.setBorder(Border.stroke(Paint.valueOf("black")));
            }
            container.add(fileDisplay, col, row);

            col++;
            if (col > 4) { // Nombre d'élément par row arbitrairement fixé à 4
                col = 0;
                row++;
            }
        }
        currentPosition();
    }

    /**
     * Au click sur le bouton de création de fichier ouvre une popup pour entrer le
     * nom du nouveau fichier
     * et si un nom est fourni lance la méthode associée de l'explorer
     */
    @FXML
    public void newFileAction() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("New File");
        dialog.setHeaderText("Create a new file");
        dialog.setContentText("Please enter file name :");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> {
            try {
                explorer.createFile(name);
                updateDisplay();
            } catch (IOException e) {

                e.printStackTrace();
            }
        });
    }

    @FXML
    public void currentPosition() {
        currentPosition.setText(null);
        currentPosition.setText(explorer.getCurrentLocation());
    }

}
