package co.simplon.promo27.model;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementTest {
    Path tempFolder;
    @BeforeEach
    public void setUp() {
        TempFolder.generate();
        tempFolder = TempFolder.temp;
        
    }

    @AfterEach
    public void reset() {
        TempFolder.cleanUp();
    }

    @Test
    void shouldNotBeDirectory() {
        Element file = new Element(tempFolder.resolve("testfile").toFile());
        assertFalse(file.isDirectory());
    }
    
    @Test
    void shouldBeDirectory() {
        //TODO: Tester avec un dossier
    }

    @Test
    void shouldRemoveFile() {
        //TODO: Tester la suppression d'un fichier
    }

    @Test
    void shouldRenameFile() {
        //TODO: Tester renommer le fichier
    }

    @Test
    void shouldNotRenameIfNameExists() {
        //TODO: Tester de renommer avec un nom de fichier déjà existant
    }
}
